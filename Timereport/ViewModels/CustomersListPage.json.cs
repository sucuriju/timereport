using System;
using Starcounter;

namespace Timereport
{
    partial class CustomersListPage : Json, IPageRouting<Func<Response>>
    {
        public Func<Response> Routing()
        {
            return () =>
            {
                return new CustomersListPage();
            };

        }

        protected override void OnData()
        {
            base.OnData();
            this.Customers.Data = Db.SQL<Customer>("SELECT p FROM Timereport.Customer p");
        }

       
    }
}
