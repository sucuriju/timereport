using Starcounter;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Timereport
{
    partial class CheckOutPage : Json, IPageRouting<Func<Response>>
    {
        public Func<Response> Routing()
        {
            return () =>
            {
                return new CheckOutPage();
            };

        }
    
        static CheckOutPage()
        {

            //DefaultTemplate.BindChildren = BindingStrategy.Bound;
        }
        protected override void OnData()
        {
            base.OnData();
            var units = TimeUnitProvider.GetTimeUnits();
            //var customerlist = new List<CustomerRowItem>();
            //var customerlist = this.Customers;
            foreach(var unit in units)
            {
                CustomerRowItem cr;
                
                if (this.Customers.Any(p => p.Customer.Data.GetObjectID() == unit.Customer.GetObjectID()))
                {
                    cr = this.Customers.First(p => p.Customer.Data.GetObjectID() == unit.Customer.GetObjectID());
                }
                else
                {
                    cr = new CustomerRowItem();
                    cr.Customer.Data = unit.Customer;
                    this.Customers.Add(cr);
                }



                ActivityRowItem activity;

                if (cr.Activities.Any(p => p.Activity.Data.GetObjectID() == unit.Activity.GetObjectID()))
                {
                    activity = cr.Activities.First(p => p.Activity.Data.GetObjectID() == unit.Activity.GetObjectID());
                }
                else
                {
                    activity = new ActivityRowItem();
                    activity.Activity.Data = unit.Activity;
                    activity.Units.Data = new List<TimeUnit>();
                    cr.Activities.Add(activity);
                }

                var unitrow = new UnitRowItem()
                {
                    Key = unit.Key,
                    TimeStamp = unit.TimeStamp.ToString(),
                    Comment = unit.Comment,
                    Duration = unit.Duration,
                    OwnerFullName = unit.OwnerFullName
                };
                
                activity.Units.Add(unitrow);

            }
            
            
            
        }

        /* Why is this method needed? If I don't call this method ReportName will be empty when calling CheckOutClick */
        void Handle(Input.ReportName action)
        {
            this.ReportName = action.Value;
        }

        void Handle(Input.CheckOutClick action)
        {
         
            // We don't allow reports without a name
            if (this.ReportName.Length == 0) { return; }

            var unitstocheckout = new List<UnitRowItem>();

            // Find all units that should be checked out and added to report.
            foreach (var customer in this.Customers)
            {
                foreach (var activity in customer.Activities)
                {
                    foreach (var unit in activity.Units)
                    {
                        if (unit.CheckOut)
                        {
                            unitstocheckout.Add(unit);
                        }
                    }

                }
            }

            if (unitstocheckout.Count() > 0)
            {
                //create a new report
                var report = ReportProvider.CreateReport(this.ReportName);
                foreach(var unit in unitstocheckout)
                {
                    //add report and checkout units. Feels like there should be a more elegant solution for this.
                    TimeUnitProvider.CheckoutTimeUnit(unit.Key, report);
                }

                //Using front-end redirect to the created report. Whould be nice is this could be backend instead.
                RedirectUrl = "/timereport/reports/" + report.Key;
            }
        }

     

        [CheckOutPage_json.Customers]
        partial class CustomerRowItem : Json
        {
            static CustomerRowItem()
            {
                DefaultTemplate.Duration.Bind = "CalculateDuration";
                DefaultTemplate.Hours.Bind = "ConvertToHours";
            }

            public string ConvertToHours
            {
                get
                {
                    return (this.Duration / 60).ToString("N1");
                }
            }

            public long CalculateDuration
            {
                get
                {
                    long sum = 0;
                    foreach (var activity in this.Activities)
                    {
                        sum += activity.Duration;
                    }
                    return sum;
                }
            }

           
            void Handle(Input.CheckOut action)
            {
                foreach (var activity in this.Activities)
                {
                    activity.CheckOut = action.Value;
                    foreach (var unit in activity.Units)
                    {
                        unit.CheckOut = action.Value;
                    }
                }
            }

        }

        [CheckOutPage_json.Customers.Activities.Units]
        partial class UnitRowItem : Json
        {
            static UnitRowItem()
            {
                DefaultTemplate.Hours.Bind = "ConvertToHours";
            }
            public string ConvertToHours
            {
                get
                {
                    return (this.Duration / 60).ToString("N1");
                }
            }
        }


        [CheckOutPage_json.Customers.Activities]
        partial class ActivityRowItem : Json
        {
            static ActivityRowItem()
            {
                DefaultTemplate.Duration.Bind = "CalculateDuration";
                DefaultTemplate.Hours.Bind = "ConvertToHours";
            }

            public string ConvertToHours
            {
                get
                {
                    return (this.Duration / 60).ToString("N1");
                }
            }

            public long CalculateDuration
            {
                get
                {
                    long sum = 0;
                    foreach (var unit in this.Units)
                    {
                        sum += unit.Duration;
                    }
                    return sum;
                }
            }

           

            void Handle(Input.CheckOut action)
            {
                foreach (var unit in this.Units)
                {
                    unit.CheckOut = action.Value;
                }
            }
        }

    }
}
