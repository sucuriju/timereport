using Starcounter;
using System;

namespace Timereport
{
    partial class ReportsListPage : Json, IPageRouting<Func<Response>>
    {
        public Func<Response> Routing()
        {
            return () =>
            {
                return new ReportsListPage();
            };

        }

        protected override void OnData()
        {
            base.OnData();
            this.Reports.Data = Db.SQL<Report>("SELECT p FROM Timereport.Report p ORDER BY p.CreatedDate DESC");
        }


    }
}
