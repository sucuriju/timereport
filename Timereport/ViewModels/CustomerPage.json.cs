using Starcounter;
using System;
using Simplified.Ring3;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Timereport
{
    partial class CustomerPage : Json, IBound<Customer>, IPageRouting<Func<string, Response>>
    {
        public Func<string, Response> Routing()
        {
            return (string id) =>
            {
                return Db.Scope(() =>
                {
                    var customer = new CustomerPage();
                    customer.SetCustomer(id == "add" ? "" : id);
                    return customer;
                });
            };

        }

        protected override void OnData()
        {
            //this.SystemUserGroups.Data = Db.SQL<SystemUserGroup>("SELECT o FROM Simplified.Ring3.SystemUserGroup o");
            //this.SystemUserGroups.Data = AvailableSystemUserGroups;
            DefaultTemplate.SystemUserGroups.Bind = "AvailableSystemUserGroups";

        }

        public IEnumerable AvailableSystemUserGroups
        {
            get
            {

                var groups = Db.SQL<SystemUserGroup>("SELECT o FROM Simplified.Ring3.SystemUserGroup o");
                return groups.Where(p => !this.Data.Groups.Any(q => q.SystemUserGroup.GetObjectID() == p.GetObjectID())).ToList();

            }
        }


        public void SetCustomer(string ID = null)
        {
            if (string.IsNullOrEmpty(ID))
            {
                this.Data = new Customer();
                var activity = new Activity()
                {
                    Customer = this.Data as Customer,
                    Name = "Default"
                };
            }
            else
            {
                var customer = Db.FromId<Customer>(ID);
                this.Data = customer;
                //this.Activities.Data = customer.Activities;
                //this.Groups = customer.Groups;
            }
        }

        void Handle(Input.SaveClick action)
        {

            this.Transaction.Commit();
            RedirectUrl = "/timereport/customers";
        }

        void Handle(Input.AddUserToGroup action)
        {
            var group = Db.FromId<SystemUserGroup>(this.SelectedSystemUserGroupsID);

            var customerGroupMember = new CustomerGroupMember();

            customerGroupMember.Customer = this.Data as Customer;
            customerGroupMember.SystemUserGroup = group;
            this.SelectedSystemUserGroupsID = null;

        }


        void Handle(Input.AddActivityClick action)
        {
            var activity = new Activity()
            {
                Customer = this.Data as Customer
            };

        }

        [CustomerPage_json.Groups]
        partial class CustomerGroupItem : Json
        {
            static CustomerGroupItem()
            {

            }
            void Handle(Input.Remove action) {

                Db.FromId(this.Key).Delete();
               
            }
        }


    }
}
