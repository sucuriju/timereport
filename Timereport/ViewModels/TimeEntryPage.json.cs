using Starcounter;
using System;
using System.Collections.Generic;
using Simplified.Ring1;
using Simplified.Ring3;
using System.Linq;
using System.Globalization;

namespace Timereport
{


    partial class TimeEntryPage : Json, IPageRouting<Func<string, Response>>
    {
        public Func<string, Response> Routing()
        {
            return (string date) =>
            {
                if (string.IsNullOrEmpty(date)) { return new TimeEntryPage(); }
                return new TimeEntryPage() { CurrentDate = date };
            };
            
        }

        public string CurrentDate;
        public List<DateTime> Dates = new List<DateTime>();
        static TimeEntryPage()
        {
            /*cannot bind to an array so calling a method for calculations instead */
            //DefaultTemplate.ColumnTotals.Bind = "CalculateTotals";
            DefaultTemplate.SystemCustomers.Bind = "SystemCustomers";
        }



        protected override void OnData()
        {

            base.OnData();

            var memberOfGroup = Db.SQL<SystemUserGroupMember>("SELECT o FROM Simplified.Ring3.SystemUserGroupMember o WHERE  o.SystemUser=?", SystemUser.GetCurrentSystemUser());
            foreach (var r in SystemUser.GetCurrentSystemUser().Groups) {
                Console.WriteLine("usergroup" + r.Name);
            }


            //get the start of the week           
            var start = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
            if (!string.IsNullOrEmpty(CurrentDate))
            {
                start = CurrentDate.ToDateTime().StartOfWeek(DayOfWeek.Monday);
            }

            //setup links
            this.NextPageUrl = "/timereport/timeentry/" + start.AddDays(7).ToShortDateString();
            this.PreviousPageUrl = "/timereport/timeentry/" + start.AddDays(-7).ToShortDateString();


            for (var i = 0; i < 7; i++)
            {
                Dates.Add(start.AddDays(i));
            }
            this.FirstDayOfWeek = Dates[0].ToString("o");
            this.LastDayOfWeek = Dates[6].ToString("o");
            var daystrings = new List<string>();
            foreach (var d in Dates)
            {
                daystrings.Add(d.ToString("o"));
            }
            
            this.Days.Data = daystrings;


            var units = TimeUnitProvider.GetCurrentUserTimeUnits(Dates[0], Dates[6]);
            //var list = new List<TimeEntryPageRowUnit>();

            /* Lets loop through all available timeunits and create a grid view.
             */

            foreach (var unit in units)
            {
                AddToNextAvaiableSlot(unit);
            }
            
            RecalculateTotals();

        }

        public IEnumerable<Customer> SystemCustomers
        {
            get
            {
                //Filter out customers that the user has access to
                var customers = Db.SQL<Customer>("SELECT p FROM Timereport.Customer p");
                var filtered_customers = new List<Customer>();
                foreach (var customer in customers)
                {
                    if (customer.Groups.Count() == 0 || customer.Groups.Any(c => SystemUser.GetCurrentSystemUser().IsMemberOfGroup(c.SystemUserGroup)))
                    {
                        filtered_customers.Add(customer);
                    }
                }
                return filtered_customers;
            }
        }

        void AddToNextAvaiableSlot(TimeUnit unit)
        {
            foreach (TimeEntryPageRow row in this.Rows)
            {
                /* Looping through each existing row to find a empty slot in the week. */
                 
                if (row.SelectedActivityKey == unit.Activity.GetObjectID())
                {
                    var slot = (unit.TimeStamp - Dates[0]).Days;
                    if (row.Units[slot].Key.Length == 0)
                    {
                        /*  This does not work, when changing an existing unit the following error is thrown:
                         *  System.Exception: ScErrDataBindingForJsonException (SCERR14012): An exception occurred when a databinding for a propert
                         *  */
                        //row.Units[slot].Data = unit;

                        /* This is a workaround for it */

                        row.Units[slot] = new TimeEntryPageRowUnit()
                        {
                            Comment = unit.Comment,
                            Key = unit.Key,
                            Duration = unit.Duration,
                            TimeStamp = unit.TimeStamp,
                            CheckedOut = unit.CheckedOut
                        };

                        row.Locked = true;
                        
                        /* slot found and filled. We are done with this unit */
                        return;
                    }
                }
            }
            
            /* No available slot in grid found. Create a new row for the customer/activity */
            AddNewRow(unit.Customer.GetObjectID(), unit.Activity.GetObjectID());

            /*Try again*/
            AddToNextAvaiableSlot(unit);


        }
        void AddNewRow(string selectedcustomerkey = null, string activitykey = null)
        {
            /* This is a mess with lots of database lookups. Is there some better solution maybe? */

            var r = new TimeEntryPageRow();

            
            r.SelectedCustomerKey = selectedcustomerkey;
            r.SelectedActivityKey = activitykey;

            if (selectedcustomerkey != null)
            {
                /* add existing customer and activity. Activity is assumed and cannot be null */
                r.Customer.Data = Db.FromId<Customer>(selectedcustomerkey);
                r.Activity.Data = Db.FromId<Activity>(activitykey);
                r.Activities.Data = Db.FromId<Customer>(selectedcustomerkey).Activities;

            } else
            {
                /* empty row, set customer and activities to first found in database */

                r.SelectedCustomerKey = SystemCustomers.First().Key;
                r.Customer.Data = SystemCustomers.First();
                var activites = Db.FromId<Customer>(SystemCustomers.First().Key).Activities;
                r.SelectedActivityKey = activites.First().Key;
                r.Activity.Data = activites.First();
                r.Activities.Data = activites;
            }

            r.Units.Data = new List<TimeEntryPageRowUnit>();
            for (var i = 0; i < 7; i++)
            {
                r.Units.Add(new TimeEntryPageRowUnit() { TimeStamp = Dates[i] });
            }
            this.Rows.Add(r);
            
        }

        /* Recalculates all column totals */
        void RecalculateTotals()
        {
            var columntotals = new long[8];
            foreach(var row in this.Rows)
            {
                for(var i = 0; i < row.Units.Count(); i++)
                {
                    columntotals[i] += row.Units[i].Duration;
                    /* last column is for grid total for all columns */
                    columntotals[7] += row.Units[i].Duration;
                }
            }
            
            /* convert to hours and string */
            this.ColumnTotals.Data = columntotals.Select(item => ((decimal)item/60).ToString("N1")).ToList();
            

        }
        

        void Handle(Input.NewRowTrigger action)
        {
            AddNewRow();
        }

      

        [TimeEntryPage_json.Rows]
        partial class TimeEntryPageRow : Json
        {
            static TimeEntryPageRow()
            {
                DefaultTemplate.Total.Bind = "RecalculateTotal";
            }
            public TimeEntryPage ParentPage
            {
                get
                {
                    return this.Parent.Parent as TimeEntryPage;
                }
            }

            protected override void OnData()
            {
                base.OnData();
                //RecalculateTotal();
            }
            void Handle(Input.SelectedCustomerKey Action)
            {
                var customer = Db.FromId<Customer>(Action.Value);
                this.Activities.Data = customer.Activities;
                this.SelectedActivityKey = customer.Activities.First().Key;
                this.Customer.Data = customer;
                this.Activity.Data = customer.Activities.First();
            }

            void Handle(Input.SelectedActivityKey Action)
            {
                this.Activity.Data = Db.FromId<Activity>(Action.Value);
            }

            public string RecalculateTotal
            {
                get
                {
                    decimal sum = 0;
                    foreach (var unit in this.Units)
                    {
                        sum += unit.Duration;
                    }
                    return (sum / 60).ToString("N1");
                }
            }

           

        }


        
        [TimeEntryPage_json.Rows.Units]
        partial class TimeEntryPageRowUnit : Json, IBound<TimeUnit>
        {
            public DateTime TimeStamp { get; set; }
            
            static TimeEntryPageRowUnit()
            {
                DefaultTemplate.Hours.Bind = "Hours";
                DefaultTemplate.IsWeekend.Bind = "IsWeekend";
            }
            public bool IsWeekend
            {
                get
                {
                    return this.TimeStamp.DayOfWeek == DayOfWeek.Saturday || this.TimeStamp.DayOfWeek == DayOfWeek.Sunday;
                }
            }

            // This is a workaround becase a number field is not nullable. We don't want to display a lot of zeroes on empty timeunits.
            public string Hours
            {
                get
                {
                    return this.Duration > 0 ? ((decimal)this.Duration / 60).ToString() : "";
                }
            }

            public TimeEntryPageRow ParentRow
            {
                get
                {
                    return this.Parent.Parent as TimeEntryPageRow;
                }
            }

            /* Update or crate a new TimeUnit when changes are made */
            public void SaveUnit()
            {
                /* lock parent row. We dont allow change of company/activity on a row with existing timeunits */
                this.ParentRow.Locked = true;

                if (this.Key != null && this.Key.Length > 0) {
                    if (this.Duration == 0)
                    {
                        TimeUnitProvider.DeleteTimeUnit(this.Key);
                        this.Key = null;
                        this.Comment = "";
                    }
                    else
                    {
                        TimeUnitProvider.UpdateTimeUnit(this.Key, this.Duration, this.Comment);
                    }
                    
                }
                else
                {
                   
                    this.Key = TimeUnitProvider.CreateTimeUnit(this.TimeStamp, this.Duration, this.Comment, this.ParentRow.SelectedCustomerKey, this.ParentRow.SelectedActivityKey).GetObjectID();
                }
                // Tell Rootpage to recalculate totals. This feels like a workaround.
                this.ParentRow.ParentPage.RecalculateTotals();
            }
           
            void Handle(Input.Comment Action) {
                this.Comment = Action.Value;  //Why is this needed?
                SaveUnit();
            }

            void Handle(Input.Hours Action)
            {
                decimal value = 0;
                bool b = Decimal.TryParse(Action.Value.Replace(",","."), NumberStyles.Any, new CultureInfo("en-US"), out value);

                this.Duration = (long)(value * 60);
                SaveUnit();
            }

        }
        

    }
}
