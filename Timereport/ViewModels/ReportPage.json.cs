using Starcounter;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Timereport
{
    partial class ReportPage : Json, IPageRouting<Func<string, Response>>
    {
        public Func<string, Response> Routing()
        {
            return (string id) =>
            {
                return Db.Scope(() =>
                {
                    var report = new ReportPage();
                    report.SetReport(id);
                    return report;
                });
            };

        }
    
        static ReportPage()
        {
            
        } 
       
        public void SetReport(string ID = null)
        {
            if (string.IsNullOrEmpty(ID))
            {
                this.Data = new Report();
                
            }
            else
            {

                var report = Db.FromId<Report>(ID);
                this.Data = report;
                ParseUnits(report.TimeUnits.OrderBy(p=>p.TimeStamp).ToList());

            }
        }

        void ParseUnits(List<TimeUnit> units)
        {
            foreach (var unit in units)
            {
                CustomerRowItem cr;

                if (this.Customers.Any(p => p.Customer.Data.GetObjectID() == unit.Customer.GetObjectID()))
                {
                    cr = this.Customers.First(p => p.Customer.Data.GetObjectID() == unit.Customer.GetObjectID());
                }
                else
                {
                    cr = new CustomerRowItem();
                    cr.Customer.Data = unit.Customer;
                    this.Customers.Add(cr);
                }



                ActivityRowItem activity;
                
                if (cr.Activities.Any(p => p.Activity.Data.GetObjectID() == unit.Activity.GetObjectID()))
                {
                    activity = cr.Activities.First(p => p.Activity.Data.GetObjectID() == unit.Activity.GetObjectID());
                }
                else
                {
                    activity = new ActivityRowItem();
                    activity.Activity.Data = unit.Activity;
                    activity.Units.Data = new List<TimeUnit>();
                    cr.Activities.Add(activity);
                }

                var unitrow = new UnitRowItem()
                {
                    Key = unit.Key,
                    TimeStamp = unit.TimeStamp.ToString(),
                    Comment = unit.Comment,
                    Duration = unit.Duration,
                    OwnerFullName = unit.OwnerFullName
                };
                activity.Units.Add(unitrow);


            }
            
        }

        void Handle(Input.SaveClick action)
        {
            
            this.Transaction.Commit();
            RedirectUrl = "/timereport/reports";
        }


        [ReportPage_json.Customers]
        partial class CustomerRowItem : Json
        {
            static CustomerRowItem()
            {
                DefaultTemplate.Duration.Bind = "CalculateDuration";
                DefaultTemplate.Hours.Bind = "ConvertToHours";
            }

            public string ConvertToHours
            {
                get
                {
                    return (this.Duration / 60).ToString("N1");
                }
            }

            public long CalculateDuration
            {
                get
                {
                    long sum = 0;
                    foreach (var activity in this.Activities)
                    {
                        sum += activity.Duration;
                    }
                    return sum;
                }
            }


           

        }

        [ReportPage_json.Customers.Activities.Units]
        partial class UnitRowItem : Json
        {
            static UnitRowItem()
            {
                DefaultTemplate.Hours.Bind = "ConvertToHours";
            }
            public string ConvertToHours
            {
                get
                {
                    return (this.Duration / 60).ToString("N1");
                }
            }
        }


        [ReportPage_json.Customers.Activities]
        partial class ActivityRowItem : Json
        {
            static ActivityRowItem()
            {
                DefaultTemplate.Duration.Bind = "CalculateDuration";
                DefaultTemplate.Hours.Bind = "ConvertToHours";
            }

            public string ConvertToHours
            {
                get
                {
                    return (this.Duration / 60).ToString("N1");
                }
            }

            public long CalculateDuration
            {
                get
                {
                    long sum = 0;
                    foreach (var unit in this.Units)
                    {
                        sum += unit.Duration;
                    }
                    return sum;
                }
            }



            
        }

    }
}
