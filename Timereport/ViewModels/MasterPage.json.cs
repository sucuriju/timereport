using Starcounter;

namespace Timereport
{
    partial class MasterPage : Json
    {
        static MasterPage()
        {
            DefaultTemplate.UserFullName.Bind = nameof(UserFullName);
        }

        public string UserFullName
        {
            get
            {
                return Simplified.Ring3.SystemUser.GetCurrentSystemUser().WhoIs.FullName;
            }
        }
        
    }
}
