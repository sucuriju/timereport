﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace Timereport
{
    internal class MainHandlers 
    {
        public void Register()
        {
            Application.Current.Use(new HtmlFromJsonProvider());
            Application.Current.Use(new PartialToStandaloneHtmlProvider());

            /* blanket denial to all pages, user must be signed in*/
            Application.Current.Use(new AuthorizedMiddleware());
            
            Handle.GET("/timereport", () => { return Self.GET("/timereport/timeentry/"); });

            /* An experimental way of routing to minimize the amount of code used. 
             * Only supports one optional parameter in this version as this app does not require more.
             * Only supports GET
             */
            RegisterRouting("/reports",         new ReportsListPage());
            RegisterRouting("/reports/{?}",     new ReportPage());
            RegisterRouting("/customers",       new CustomersListPage());
            RegisterRouting("/customers/{?}",   new CustomerPage());
            RegisterRouting("/timeentry/{?}",   new TimeEntryPage());
            RegisterRouting("/checkout",        new CheckOutPage());


            /* starcounter default way saved incase the above implodes.. */

            //Handle.GET("/timereport/timentry", () => { return Self.GET("/timereport/timeentry/"); });
            //Handle.GET("/timereport/customers", () => { return Self.GET("/timereport/customers/"); });
            //Handle.GET("/timereport/reports", () => { return Self.GET("/timereport/reports/"); });

            /*
            Handle.GET("/timereport/customers/{?}", (string id) => WrapPage("/timereport/partial/customers/"+id));
            Handle.GET("/timereport/partial/customers/{?}", (string id) =>
            {
                if (string.IsNullOrEmpty(id)){return new CustomersListPage();}
                return Db.Scope(() =>
                {
                    var customer = new CustomerPage();
                    customer.SetCustomer(id == "add" ? "" : id);
                    return customer;
                });
            });
            */
            /*
             Handle.GET("/timereport/reports/{?}", (string id) => WrapPage("/timereport/partial/reports/" + id));
             Handle.GET("/timereport/partial/reports/{?}", (string id) =>
             {
                 if (string.IsNullOrEmpty(id)){return new ReportsListPage();}

                 return Db.Scope(() =>
                 {
                     var report = new ReportPage();
                     report.SetReport(id);
                     return report;
                 });
             });
             */



            /*
            Handle.GET("/timereport/timeentry/{?}", (string date) => WrapPage("/timereport/partial/timeentry/"+date));
            Handle.GET("/timereport/partial/timeentry/{?}", (string date) =>
            {
                if (string.IsNullOrEmpty(date)){return new TimeEntryPage();}
                return new TimeEntryPage() { CurrentDate=date};
            });
            */
            /*
            Handle.GET("/timereport/checkout", () => WrapPage("/timereport/partial/checkout"));
            Handle.GET("/timereport/partial/checkout", () =>
            {
                return new CheckOutPage();
            });
            */





        }

        /* experimental routing that registers partial and default view int the same method.
         * Routing function is fetched from the page viewmodel class. Not sure if crazy or not.
         * Should be possible to optimize more. */

        public static void RegisterRouting(string path, IPageRouting<Func<string, Response>> page)
        {
            Handle.GET("/timereport" + path, (string p) => WrapPage("/timereport/partial" + path.Replace("{?}",p)));
            Handle.GET("/timereport/partial" + path, page.Routing());
        }
        public static void RegisterRouting(string path, IPageRouting<Func<Response>> page)
        {
            Handle.GET("/timereport" + path, () => WrapPage("/timereport/partial" + path));
            Handle.GET("/timereport/partial" + path, page.Routing());
        }

        /* experiments 

        public static void RegisterRouting(string path, Func<Response> func)
        {
            Handle.GET("/timereport" + path, () => WrapPage("/timereport/partial" + path));
            Handle.GET("/timereport/partial" + path, func);
        }
        public static void RegisterRouting<T>(string path, Func<T, Response> func)
        {
            Handle.GET("/timereport" + path, (string param) => WrapPage("/timereport/partial" + path.Replace("{?}",param)));
            Handle.GET("/timereport/partial" +path, func);
        }

        */
        public static Json WrapPage(string partialPath) 
        {
            var master = GetMasterPageFromSession();
            master.CurrentPage = Self.GET(partialPath);
            if (master.CurrentPage.Data == null)
            {
                master.CurrentPage.Data = null; 
            }
            return master;
        }

        public static MasterPage GetMasterPageFromSession()
        {

            MasterPage master = Session.Ensure().Store[nameof(MasterPage)] as MasterPage;
            if (master == null)
            {
                master = new MasterPage();
                Session.Current.Store[nameof(MasterPage)] = master;
            }
            
            return master;
            
        }


    }
}
