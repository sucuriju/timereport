﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Simplified.Ring3;
using Starcounter;
using Simplified.Ring1;


namespace Timereport
{
    [Database]
    public class TimeUnit
    {
        public Something Owner { get; set; }
        public string OwnerFullName {
            get {
                return (Owner as SystemUser).WhoIs.FullName;
            }
        }
        public string Key
        {
            get
            {
                return this.GetObjectID();
            }
        }
        public long Duration { get; set; }
        public string Comment { get; set; }
        public DateTime TimeStamp { get; set; }
        public Customer Customer { get; set; }
        public Activity Activity { get; set; }
        public bool CheckedOut { get; set; }
        public Report Report { get; set; }

    }


    class TimeUnitProvider
    {
        public static TimeUnit CreateTimeUnit(DateTime timestamp, Int64 duration, string comment, string customerId, string activityId) {

            return Db.Transact(() =>
            {
                var unit = new TimeUnit();
                unit.Owner = SystemUser.GetCurrentSystemUser();
                unit.TimeStamp = timestamp;
                unit.Duration = duration;
                unit.Comment = comment;
                unit.Customer = Db.FromId<Customer>(customerId);
                unit.Activity = Db.FromId<Activity>(activityId);
                return unit;
                
            });



        }
        public static void DeleteTimeUnit(string id)
        {
            Db.Transact(() =>
            {
                
                Db.FromId<TimeUnit>(id).Delete();
                
            });


        }
        public static void UpdateTimeUnit(string id, long duration, string comment)
        {
            var unit = Db.FromId<TimeUnit>(id);
            
            Db.Transact(() =>
            {
                unit.Duration = duration;
                unit.Comment = comment;

            });
            
        }
        public static void CheckoutTimeUnit(string id, Report report)
        {
            var unit = Db.FromId<TimeUnit>(id);
            
            Db.Transact(() =>
            {
                unit.Report = report;
                unit.CheckedOut = true;

            });
            
        }

        public static List<TimeUnit> GetCurrentUserTimeUnits(DateTime fromdate, DateTime todate)
        {
            var allNotes = Db.SQL<TimeUnit>("SELECT n FROM TimeUnit n WHERE n.Owner = ?", SystemUser.GetCurrentSystemUser());
            return allNotes.Where(x => x.TimeStamp.Date >= fromdate.Date && x.TimeStamp.Date <= todate.Date).OrderBy(p => p.TimeStamp).ToList();


        }

        public static List<TimeUnit> GetTimeUnits(DateTime fromdate, DateTime todate)
        {
            var allNotes = Db.SQL<TimeUnit>("SELECT n FROM TimeUnit n");
            return allNotes.Where(x => x.CheckedOut == false && x.TimeStamp.Date >= fromdate.Date && x.TimeStamp.Date <= todate.Date).OrderBy(p => p.TimeStamp).ToList();
        }
        public static List<TimeUnit> GetTimeUnits()
        {
            return GetTimeUnits(DateTime.MinValue, DateTime.MaxValue);

        }
    }
}
