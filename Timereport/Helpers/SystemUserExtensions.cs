﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Simplified.Ring3;
using Starcounter;

namespace Timereport
{
    public static class SystemUserExtensions
    {

        public static bool IsMemberOfGroup(this SystemUser systemUser, SystemUserGroup systemUserGroup)
        {
            return systemUser.Groups.Any(g => g.Key == systemUserGroup.Key);
        }
    }
}
