﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;
using Simplified.Ring1;
using Simplified.Ring3;

namespace Timereport
{
    [Database]
    public class Customer
    {
        public string Name { get; set; }

        public QueryResultRows<Activity> Activities => Db.SQL<Activity>("select e from Timereport.Activity e where e.Customer = ?", this);
        public QueryResultRows<CustomerGroupMember> Groups => Db.SQL<CustomerGroupMember>("select e from Timereport.CustomerGroupMember e where e.Customer = ?", this);
        public string Key
        {
            get
            {
                return this.GetObjectID();
            }
        }

    }

    [Database]
    public class CustomerGroupMember
    {
        public Customer Customer;
        public SystemUserGroup SystemUserGroup;
        public string Key
        {
            get
            {
                return this.GetObjectID();
            }
        }

    }
    class CustomerProvider
    {
    }
}
