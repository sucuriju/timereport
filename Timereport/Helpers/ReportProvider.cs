﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace Timereport
{

    [Database]
    public class Report
    {
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Key
        {
            get
            {
                return this.GetObjectID();
            }
        }
        public QueryResultRows<TimeUnit> TimeUnits => Db.SQL<TimeUnit>("SELECT e FROM TimeUnit e WHERE e.Report = ?", this);

    }

    public class ReportProvider
    {

        public static Report CreateReport(string name)
        {

            return Db.Transact(() =>
            {
                return new Report() { Name=name, CreatedDate=DateTime.Now};

            });

        }

    }
}
