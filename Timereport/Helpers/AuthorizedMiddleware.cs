﻿using Simplified.Ring3;
using Starcounter;
using System;

namespace Timereport
{
    public class AuthorizedMiddleware : IMiddleware
    {

        public static bool IsSignedIn()
        {
            var user = SystemUser.GetCurrentSystemUser();
            return (user != null);
        }

        public void Register(Application application)
        {
            application.Use((Request request) =>
            {
                Console.WriteLine("signedin" + IsSignedIn());

                if (request.Uri.ToLower().StartsWith("/timereport") && !IsSignedIn())
                {
                    var resp = new Response()
                    {

                        StatusCode = 302

                    };
                    /* fugly solution but it gets the work done. Trials with blending did not work, so need to look into it in the future. */
                    resp.Headers["Location"] = "/signin/signinuser?%2ftimereport";
                    return resp;
                }
                return null;
            });
        }
    }
}