﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace Timereport
{

    [Database]
    public class Activity
    {
        public string Name { get; set; }
        public Customer Customer { get; set; }
        public string Key
        {
            get
            {
                return this.GetObjectID();
            }
        }

    }

    class ActivityProvider
    {
    }
}
